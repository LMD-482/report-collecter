<?php

use Illuminate\Support\Facades\Route;
use Workable\ReportCollecter\Http\Controllers\ApiReporter;

Route::group([
    'prefix'     => 'report-collecter',
    'namespace'  => 'Workable\ReportCollecter\Http\Controllers;',
], function () {

    Route::get('/get-public-keyword', [ApiReporter::class, 'keywordPublic'])->name('get.public.keyword');

});
