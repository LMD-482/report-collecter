<?php

namespace Workable\ReportCollecter\Http\Controllers;


use Illuminate\Http\Request;
use Workable\ReportCollecter\Services\ReportCollecterService;
use Workable\ReportCollecter\Traits\ResponseTrait;

class ApiReporter
{

    use ResponseTrait;
    private  $reporterService;

    public function __construct(ReportCollecterService $reporterService)
    {
        $this->reporterService = $reporterService;
    }

    public function keywordPublic(){

        $data = $this->reporterService->getKeywordPublic();

        return $this->respondSuccess("Success", $data);
    }








}
