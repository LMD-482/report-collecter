<?php

namespace Workable\ReportCollecter;

use Illuminate\Support\ServiceProvider;

class ReportCollecterServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
        $this->commands([

        ]);

        $this->app->booted(function () {

        });

    }

    public function register()
    {
        parent::register();
    }

    protected function _registerConfig()
    {

    }


}
