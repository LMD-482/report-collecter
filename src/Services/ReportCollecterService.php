<?php

namespace Workable\ReportCollecter\Services;


use Illuminate\Support\Facades\DB;

class ReportCollecterService
{
    public function __construct()
    {

    }

    public function getKeywordPublic()
    {
        $todayCount = DB::table('keywords')->whereDate('created_at', now()->toDateString())->count();

        return [
            'total' => $todayCount
        ];
    }


}
